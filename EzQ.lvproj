﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="EzQ Example.vi" Type="VI" URL="../Source/EzQ Example.vi"/>
		</Item>
		<Item Name="Typedefs" Type="Folder">
			<Item Name="EzQ message cluster.ctl" Type="VI" URL="../Source/EzQ message cluster.ctl"/>
			<Item Name="EzQ Registration cluster.ctl" Type="VI" URL="../Source/EzQ Registration cluster.ctl"/>
		</Item>
		<Item Name="Register EzQ.vi" Type="VI" URL="../Source/Register EzQ.vi"/>
		<Item Name="Send to EzQ.vi" Type="VI" URL="../Source/Send to EzQ.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="Close TCP Messenger.vi" Type="VI" URL="../Close TCP Messenger.vi"/>
			<Item Name="Create TCP Client.vi" Type="VI" URL="../Create TCP Client.vi"/>
			<Item Name="Create TCP Server.vi" Type="VI" URL="../Create TCP Server.vi"/>
			<Item Name="Exitimer.lvclass" Type="LVClass" URL="../../exitimer/Exitimer.lvclass"/>
			<Item Name="EzTCP.lvclass" Type="LVClass" URL="../EzTCP.lvclass"/>
			<Item Name="New.vi" Type="VI" URL="../../exitimer/New.vi"/>
			<Item Name="Receive TCP Message.vi" Type="VI" URL="../Receive TCP Message.vi"/>
			<Item Name="Send TCP Message.vi" Type="VI" URL="../Send TCP Message.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
