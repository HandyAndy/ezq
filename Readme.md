# EzQ
## Description

This is a small library for standardizing message passing queues.

The intent was to use it with EzTCP for dependency decoupling.  As such, it deserves to be its own module.  We'll see where it goes from there.


## Some Details

Written in LabVIEW 2019

No other dependencies

## License

MIT License, since it really is a detached appendage of EzTCP